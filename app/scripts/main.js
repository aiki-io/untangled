var game = (function () {
    'use strict';


    var my = {
        circles: [],
        lines: [],
        thinLineThickness: 1,
        boldLineThickness: 5,
        currentLevel: 0,
        progress: 0
    };

    my.levels = [
        {
            circles: [
                {x: 400, y: 156},
                {x: 381, y: 241},
                {x: 84, y: 233},
                {x: 88, y: 73}],
            relationship: [
                {connectedPoints: [1, 2]},
                {connectedPoints: [0, 3]},
                {connectedPoints: [0, 3]},
                {connectedPoints: [1, 2]}
            ]
        },
        {
            circles: [
                {x: 401, y: 73},
                {x: 400, y: 240},
                {x: 88, y: 241},
                {x: 84, y: 72}],
            relationship: [
                {connectedPoints: [1, 2, 3]},
                {connectedPoints: [0, 2, 3]},
                {connectedPoints: [0, 1, 3]},
                {connectedPoints: [0, 1, 2]}
            ]
        },
        {
            circles: [
                {x: 192, y: 155},
                {x: 353, y: 109},
                {x: 493, y: 156},
                {x: 490, y: 236},
                {x: 348, y: 276},
                {x: 195, y: 228}],
            relationship: [
                {connectedPoints: [2, 3, 4]},
                {connectedPoints: [3, 5]},
                {connectedPoints: [0, 4, 5]},
                {connectedPoints: [0, 1, 5]},
                {connectedPoints: [0, 2]},
                {connectedPoints: [1, 2, 3]}
            ]
        }
    ];


    my.drawCircle = function (circle) {
        my.ctx.fillStyle = 'gold';
        my.ctx.beginPath();
        my.ctx.arc(circle.start.x, circle.start.y, circle.radius, 0, Math.PI * 2);
        my.ctx.fill();
    };

    my.drawLine = function (line) {
        my.ctx.beginPath();
        my.ctx.moveTo(line.start.x, line.start.y);
        my.ctx.lineTo(line.end.x, line.end.y);
        my.ctx.lineWidth = line.thickness;
        my.ctx.strokeStyle = '#cfc';
        my.ctx.stroke();
    };

    my.clear = function () {
        my.ctx.clearRect(0, 0, my.width, my.height);
    };

    my.Circle = function (start, radius) {
        this.start = start;

        this.radius = radius;
    };

    my.Line = function (start, end, thickness) {
        this.start = start;
        this.end = end;
        this.thickness = thickness;

    };

    my.isInBetween = function (a, b, c) {
        // return false if b is almost equal to a or c.
        // this is to eliminate some floating point when
        // two value is equal to each other but different with 0.00000...0001
        if (Math.abs(a - b) < 0.000001 || Math.abs(b - c) < 0.000001) {
            return false;
        }

        // true when b is in between a and c
        return (a < b && b < c) || (c < b && b < a);
    };

    my.isIntersect = function (line1, line2) {
        // convert line1 to general form of line: Ax+By = C
        var a1 = line1.end.y - line1.start.y;
        var b1 = line1.start.x - line1.end.x;
        var c1 = a1 * line1.start.x + b1 * line1.start.y;

        // convert line2 to general form of line: Ax+By = C
        var a2 = line2.end.y - line2.start.y;
        var b2 = line2.start.x - line2.end.x;
        var c2 = a2 * line2.start.x + b2 * line2.start.y;

        // calculate the intersection point
        var d = a1 * b2 - a2 * b1;

        // parallel when d is 0
        if (d === 0) {
            return false;
        }

        // solve the interception point at (x, y)
        var x = (b2 * c1 - b1 * c2) / d;
        var y = (a1 * c2 - a2 * c1) / d;

        // check if the interception point is on both line segments
        if ((my.isInBetween(line1.start.x, x, line1.end.x) || my.isInBetween(line1.start.y, y, line1.end.y)) &&
            (my.isInBetween(line2.start.x, x, line2.end.x) || my.isInBetween(line2.start.y, y, line2.end.y))) {
            return true;
        }

        // be default the given lines is not intersected.
        return false;
    };


    my.detectIntersections = function () {
        // checking lines intersection and bold those lines.
        var total = 0;
        for (var i = 0; i < my.lines.length; i++) {
            var line1 = my.lines[i];
            line1.thickness = my.thinLineThickness;

            for (var j = 0; j < i; j++) {
                var line2 = my.lines[j];

                // we check if two lines are intersected,
                // and bold the line if they are.
                if (my.isIntersect(line1, line2)) {
                    line1.thickness = my.boldLineThickness;
                    line2.thickness = my.boldLineThickness;
                    total++;
                }
            }
        }
        return total;
    };

    my.setupLevel = function () {
        my.circles = [];
        my.lines = [];
        var level = my.levels[my.currentLevel];
        for (var i = 0; i < level.circles.length; ++i) {
            var start = {
                x: level.circles[i].x,
                y: level.circles[i].y
            };
            my.circles.push(new my.Circle(start, 10));
        }
        my.progress = 0;
        my.connectCircles();
        my.drawAllLines();
        my.detectIntersections();
        my.updateProgress();
    };




    my.connectCircles = function () {
       var level = my.levels[my.currentLevel];
        my.lines = [];
        for (var i in level.relationship) {
            var pts = level.relationship[i].connectedPoints;
            var circleA = my.circles[i];
            for (var j in pts) {
                var circleB = my.circles[pts[j]];
                my.lines.push(new my.Line(circleA.start, circleB.start, my.thinLineThickness));
            }
        }
    };

    my.drawAllLines = function () {
        for (var i = 0; i < my.lines.length; ++i) {
            my.drawLine(my.lines[i]);
        }
    };

    my.drawAllCircles = function () {
        for (var i = 0; i < my.circles.length; ++i) {
            my.drawCircle(my.circles[i]);
        }


    };

    my.updateProgress = function () {
        var progress = 0;
        for (var i = 0; i < my.lines.length; ++i) {
            if (my.lines[i].thickness === my.thinLineThickness) {
                progress += 1;
            }
            var percentage = Math.floor(progress / my.lines.length * 100);
            my.progress = percentage;
            $('#progress').text(percentage);
            $('level').text(my.currentLevel);
        }
    };

    my.checkLevel = function () {
        if (my.progress === 100) {
            if (my.currentLevel + 1 < my.levels.length) {
                my.currentLevel += 1;
                my.setupLevel();
            }
        }
    };

    my.gameloop = function () {
        my.clear();
        my.drawAllCircles();
        my.drawAllLines();
    };

    my.init = function () {
        var canvas = document.getElementById('game');
        my.width = canvas.width;
        my.height = canvas.height;
        my.ctx = canvas.getContext('2d');

        $('#game').bind('mousedown touchstart', function (e) {

            var touch = e.originalEvent.touches && e.originalEvent.touches[0];
            var pageX = (touch || e).pageX;
            var pageY = (touch || e).pageY;

            var canvasPosition = $(this).offset();
            var mouseX = pageX - canvasPosition.left;
            var mouseY = pageY - canvasPosition.top;
            for (var i = 0; i < my.circles.length; ++i) {
                var circleX = my.circles[i].start.x;
                var circleY = my.circles[i].start.y;
                var radus = my.circles[i].radius;
                if (Math.pow(mouseX - circleX, 2) + Math.pow(mouseY - circleY, 2) < Math.pow(radus, 2)) {
                    my.targetCircleIndex = i;
                    break;
                }
            }
        });
        $('#game').bind('mouseup touchend', function () {
            my.targetCircleIndex = undefined;
            my.checkLevel();
        });
        $('#game').bind('mousemove touchmove', function (e) {
            e.preventDefault();
            var touch = e.originalEvent.touches && e.originalEvent.touches[0];
            var pageX = (touch || e).pageX;
            var pageY = (touch || e).pageY;

            var canvasPosition = $(this).offset();
            var mouseX = pageX - canvasPosition.left;
            var mouseY = pageY - canvasPosition.top;
            if (my.targetCircleIndex !== undefined) {

                var circle = my.circles[my.targetCircleIndex];
                circle.start.x = mouseX;
                circle.start.y = mouseY;
                my.connectCircles();
                my.drawAllLines();
                my.detectIntersections();
                my.updateProgress();


            }
        });

        setInterval(my.gameloop, 30);
    };

    return my;
})();

$(document).ready(function () {
    'use strict';
    game.init();
    game.setupLevel();

});
